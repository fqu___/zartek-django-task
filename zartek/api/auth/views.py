from rest_framework_simplejwt.views import TokenObtainPairView
from api.auth.serializers import UserTokenObtainPairSerializer


class UserTokenObtainPairView(TokenObtainPairView):
    serializer_class = UserTokenObtainPairSerializer
