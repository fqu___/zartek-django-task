from user.models import PostingUser, BookingUser
from api.user.serializers import PostingUserSerializer, BookingUserSerializer
from rest_framework.decorators import api_view, permission_classes, renderer_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework import status
from api.general.functions import generate_serializer_errors
from django.contrib.auth.models import User


@api_view(['POST'])
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def create_posting_user(request):
	serialized = PostingUserSerializer(data=request.data)
	if serialized.is_valid():
		username = request.POST['username']
		email = request.POST['email']
		password = request.POST['password']
		name = serialized.data['name']

		if User.objects.filter(username=username).exists():
			response_data = {
				'status' : 6001,
				'message' : "Posting User already exists"
			}
			return Response(response_data, status=status.HTTP_200_OK)
		else:
			user = User.objects.create_user(username=username,email=email,password=password)
			PostingUser.objects.create(name=name,user=user)         
			response_data = {
				'statusCode' :6000,
				'message' : "Posting User Created"
			}
			return Response(response_data, status=status.HTTP_200_OK)
	else:
		response_data = {
			'statusCode' : 6002,
			'message' : generate_serializer_errors(serialized._errors)
		}    
		return Response(response_data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def create_booking_user(request):
	serialized = BookingUserSerializer(data=request.data)
	if serialized.is_valid():
		username = request.POST['username']
		email = request.POST['email']
		password = request.POST['password']
		name = serialized.data['name']

		if User.objects.filter(username=username).exists():
			response_data = {
				'status' : 6001,
				'message' : "Booking User already exists"
			}
		else:
		   	user =  User.objects.create_user(
				username = username,
				email = email,
				password = password
			)


		BookingUser.objects.create(
			name = name,
			user = user
		)         
		response_data = {
			'statusCode' :6000,
			'message' : "Booking User Created",
		}
		return Response(response_data, status=status.HTTP_200_OK)
	else:
		response_data = {
			'statusCode' : 6001,
			'message' : generate_serializer_errors(serialized._errors)
		}    
		return Response(response_data, status=status.HTTP_200_OK)        