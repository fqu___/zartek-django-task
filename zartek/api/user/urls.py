
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from api.user import views

app_name = "api-users"


urlpatterns = [
    path('create-posting-user/', views.create_posting_user, name='create_posting_user'),
    path('create-booking-user/', views.create_booking_user, name='create_booking_user'),
]
