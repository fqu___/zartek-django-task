from rest_framework import serializers
from user.models import PostingUser, BookingUser


class PostingUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostingUser
        fields = "__all__"


class BookingUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = BookingUser
        fields = "__all__"
