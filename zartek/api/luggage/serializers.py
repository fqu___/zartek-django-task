from rest_framework import serializers
from luggage.models import PostLuggage, BookLuggage


class PostingLuggageSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostLuggage
        fields = "__all__"


class BookingLuggageSerializer(serializers.ModelSerializer):
    class Meta:
        model = BookLuggage
        fields = "__all__"        