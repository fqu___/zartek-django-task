from user.models import PostingUser, BookingUser
from luggage.models import BookLuggage, PostLuggage
from api.luggage.serializers import PostingLuggageSerializer, BookingLuggageSerializer
from rest_framework.decorators import api_view, permission_classes, renderer_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework import status
from api.general.functions import generate_serializer_errors, get_current_role
from django.contrib.auth.models import User
from django.core import serializers
import razorpay
client = razorpay.Client(auth=("rzp_test_orkAE0WAceXCyj", "vnQ1aD4Sk8kmyvMtjg9hZgen"))


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@renderer_classes((JSONRenderer,))
def post_luggage(request):
	role = get_current_role(request)
	serialized =  PostingLuggageSerializer(data=request.data)

	if role == "postinguser":
		if serialized.is_valid():
			serialized.save()

			response_data = {
				"StatusCode": 6000,
				'data' : serialized.data
			}
			return Response(response_data,status=status.HTTP_200_OK)
		else:
			response_data = {
				"StatusCode": 6001,
				'message' : generate_serializer_errors(serialized._errors)
			}
			return Response(response_data,status=status.HTTP_200_OK)
	else:
		response_data = {
			"StatusCode": 6001,
			'message' : "You don't have permission to do this"
		}
		return Response(response_data,status=status.HTTP_200_OK)	


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
@renderer_classes((JSONRenderer,))
def book_luggage(request):
	role = get_current_role(request)	
	serialized =  BookingLuggageSerializer(data=request.data)
	user = request.user.username
	print(user)

	if role == "bookinguser":
		if serialized.is_valid(): 
			all_luggages = list(PostLuggage.objects.values_list('luggage',flat=True))
			user_luggages = request.POST['luggage_type']
			
			split_luggage = user_luggages.split(',')  #if luggage items are separated by comma in app
			list_luggages= list(split_luggage)

			matched_items = []
			for i in list_luggages:
				for j in all_luggages:
					if i == j:
						matched_items.append(i)

			if matched_items == list_luggages:
				total = 0 
				for i in matched_items:
					item = PostLuggage.objects.get(luggage=i)
					item_price = item.luggage_cost
					total +=item_price

				serialized.save()

				pk = serialized.data['id']
				instance = BookLuggage.objects.get(id=pk)	
				instance.fare = total
				instance.user = user
				instance.save()



				DATA = {
					"amount" : 100*total,
					"currency" : 'INR',
					"payment_capture"  : 1,
					"receipt" : "receipt#001",
				}
				client.order.create(data=DATA)		

				response_data = {
					'statusCode' :6000,
					'message' : "Luggage Booked"
				}
				return Response(response_data, status=status.HTTP_200_OK)

			else:	
				response_data = {
					'statusCode' :6000,
					'message' : "Invalid Luggage Items"
				}
				return Response(response_data, status=status.HTTP_200_OK)	

		else:
			response_data = {
				'statusCode' :6001,
				'message' : "Invalid Entry"
			}
			return Response(response_data, status=status.HTTP_200_OK)
	else:
		response_data = {
			'statusCode' :6001,
			'message' : "You don't have permission to do this"
		}	
		return Response(response_data, status=status.HTTP_200_OK)
