from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from api.luggage import views


app_name = "api-luggage"


urlpatterns = [
    path('post-luggage/', views.post_luggage, name='post_luggage'),
    path('book-luggage/', views.book_luggage, name='book_luggage'),
]
