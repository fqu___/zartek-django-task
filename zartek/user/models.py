from django.db import models

# Create your models here.


class PostingUser(models.Model):
    name = models.CharField(max_length=50)
    user = models.OneToOneField('auth.User',related_name="postinguser",blank=True,null=True,on_delete=models.CASCADE)

class BookingUser(models.Model):
    name = models.CharField(max_length=50)
    user = models.OneToOneField ('auth.User',related_name="bookinguser",blank=True,null=True,on_delete=models.CASCADE)

