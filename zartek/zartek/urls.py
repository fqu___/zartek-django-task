
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api/api-auth/', include('rest_framework.urls')),
    url(r'^api/auth/', include('api.auth.urls')),
    url(r'^api/user/', include('api.user.urls')),
    url(r'^api/luggage/', include('api.luggage.urls')),
]
