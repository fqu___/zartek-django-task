from django.db import models
import json
from django.contrib.postgres.fields import ArrayField

# Create your models here.
class BookLuggage(models.Model):
	user = models.CharField(max_length=100,blank=True,null=True)
	luggage_type = models.CharField(max_length=100)
	fare = models.IntegerField(blank=True,null=True)
	limit = models.ForeignKey('PostLuggage',blank=True,null=True,on_delete=models.CASCADE)

	def get_luggage_type(self):
		return json.loads(self.luggage_type)

	def set_luggage_type(self, types):
		self.luggage_type = json.dumps(types)

		

	def __str__(self):
		return str(self.limit)	


class PostLuggage(models.Model):
	luggage = models.CharField(max_length=50)
	luggage_cost = models.PositiveIntegerField()
	luggage_limit = models.PositiveIntegerField()
	active = models.BooleanField(default=True,blank=True,null=True)
	user_book_limit = models.IntegerField(blank=True,null=True)

	def __str__(self):
		return str(self.user_book_limit)