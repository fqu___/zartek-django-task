# Zartek Django Task

**Create virtualenv by `virtualenv venv -p python3`**
**Required packages**`pip install -r req.txt`

* [ ] Initially create posting user followed by booking user.
    * form-data keys:
    1. name
    2. username
    3. password
    4. email
* [ ] Create Luggage items by entering posting user's credentials.
    *  form-data keys:
        1. luggage
        2. luggage_cost
        3. luggage_limit

* [ ] Create booking items by entering booking user's credentials.
    *  form-data keys:
        1.  luggage_type (items separated by comma)


